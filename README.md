A shitty little converter from Forge BlockState -> vanilla. Enjoy...also doesn't work on multiblock

* if your file doesn't have forge_marked it will be skipped
* if your file is multiblock it will be skipped
* if your file is not a json it will be skipped
 
## Usage
1.  Make a backup of your Blockstates. this is a destuctive process
2.  Plop the converter.py in your blockstates folder
3.  Make another backup because I kinda eyeballed the syntax "fixer" at the end.
4.  python .\converter.py
5.  Celebrate or Restore backup

<3 Biom4st3r

![Hits](https://hitcounter.pythonanywhere.com/count/tag.svg?url=https%3A%2F%2Fgitlab.com%2Fbiom4st3r%2Fforge-to-vanilla-blockstate)